@extends('layout.master')

@section('judul')
    <h4>Edit Data Pemeran</h4>
@endsection

@section('isi')
<form action="/cast/{{ $cast->id }}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label for="nama">Nama</label>
      <input name="nama" value="{{ $cast->nama }}" type="text" class="form-control" id="nama" aria-describedby="emailHelp" placeholder="Masukan Nama Pemain">
    </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label for="umur">Umur</label>
      <input name="umur" value="{{ $cast->umur }}" type="number" class="form-control" id="umur" placeholder="Masukan Umur">
    </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
        <label for="bio">Biografi</label>
        <textarea name="bio" type="text" class="form-control" id="umur" placeholder="Biografi">{{ $cast->bio }}</textarea>
      </div>
      @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    
    <button type="submit" class="btn btn-primary">Update</button>
  </form>
@endsection