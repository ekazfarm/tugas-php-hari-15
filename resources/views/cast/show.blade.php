@extends('layout.master')

@section('judul')
    <h4>Pemeran Film</h4>
@endsection

@section('isi')
    <h1 class="text-center">{{ $cast->nama }}</h1><br>
    <p class="paragraf">{{ $cast->bio }}</p>

    <a href="/cast" class="btn btn-info mt-auto">Back</a>
@endsection