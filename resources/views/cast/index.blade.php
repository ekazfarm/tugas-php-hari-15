@extends('layout.master')

@section('judul')
    <h4>Data Pemeran Film</h4>
@endsection

@section('isi')
<a href="/cast/create" class="btn btn-primary mb-3">Tambah Data</a>
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
     @forelse ($cast as $key => $item)
         <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $item->nama }}</td>
            <td>{{ $item->umur }}</td>
            <td>{{ $item->bio }}</td>
            <td>
                
                <form action="/cast/{{ $item->id }}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{ $item->id }}" class="btn btn-warning btn-sm">Detail</a>
                    <a href="/cast/{{ $item->id }}/edit" class="btn btn-success btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
         </tr>
     @empty
         <tr>
             <td>Data Masih Kosong</td>
         </tr>
     @endforelse
    </tbody>
  </table>

@endsection